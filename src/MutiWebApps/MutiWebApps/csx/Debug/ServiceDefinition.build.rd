﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="MutiWebApps" generation="1" functional="0" release="0" Id="0f54a34f-f816-4c86-9e71-8856d4f9f8d3" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="MutiWebAppsGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="FrontEnd:FrontendEndpoint" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/MutiWebApps/MutiWebAppsGroup/LB:FrontEnd:FrontendEndpoint" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="BackEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/MutiWebApps/MutiWebAppsGroup/MapBackEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BackEndInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/MutiWebApps/MutiWebAppsGroup/MapBackEndInstances" />
          </maps>
        </aCS>
        <aCS name="FrontEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/MutiWebApps/MutiWebAppsGroup/MapFrontEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="FrontEndInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/MutiWebApps/MutiWebAppsGroup/MapFrontEndInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:FrontEnd:FrontendEndpoint">
          <toPorts>
            <inPortMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEnd/FrontendEndpoint" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:BackEnd:BackendEndpoint">
          <toPorts>
            <inPortMoniker name="/MutiWebApps/MutiWebAppsGroup/BackEnd/BackendEndpoint" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapBackEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/MutiWebApps/MutiWebAppsGroup/BackEnd/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBackEndInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/MutiWebApps/MutiWebAppsGroup/BackEndInstances" />
          </setting>
        </map>
        <map name="MapFrontEnd:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEnd/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapFrontEndInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEndInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="BackEnd" generation="1" functional="0" release="0" software="D:\projects\ainetclient\src\MutiWebApps\MutiWebApps\csx\Debug\roles\BackEnd" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="BackendEndpoint" protocol="http" />
              <outPort name="BackEnd:BackendEndpoint" protocol="http">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/MutiWebApps/MutiWebAppsGroup/SW:BackEnd:BackendEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;BackEnd&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BackEnd&quot;&gt;&lt;e name=&quot;BackendEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;FrontEnd&quot;&gt;&lt;e name=&quot;FrontendEndpoint&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/MutiWebApps/MutiWebAppsGroup/BackEndInstances" />
            <sCSPolicyFaultDomainMoniker name="/MutiWebApps/MutiWebAppsGroup/BackEndFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="FrontEnd" generation="1" functional="0" release="0" software="D:\projects\ainetclient\src\MutiWebApps\MutiWebApps\csx\Debug\roles\FrontEnd" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="FrontendEndpoint" protocol="http" portRanges="80" />
              <outPort name="BackEnd:BackendEndpoint" protocol="http">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/MutiWebApps/MutiWebAppsGroup/SW:BackEnd:BackendEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;FrontEnd&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BackEnd&quot;&gt;&lt;e name=&quot;BackendEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;FrontEnd&quot;&gt;&lt;e name=&quot;FrontendEndpoint&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEndInstances" />
            <sCSPolicyFaultDomainMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEndFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyFaultDomain name="BackEndFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="FrontEndFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BackEndInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="FrontEndInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="86ad557e-e2bb-4a7e-bf54-305663ff8bb3" ref="Microsoft.RedDog.Contract\ServiceContract\MutiWebAppsContract@ServiceDefinition.build">
      <interfacereferences>
        <interfaceReference Id="397b2bd1-b35d-470d-82cf-71086f077b37" ref="Microsoft.RedDog.Contract\Interface\FrontEnd:FrontendEndpoint@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/MutiWebApps/MutiWebAppsGroup/FrontEnd:FrontendEndpoint" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>