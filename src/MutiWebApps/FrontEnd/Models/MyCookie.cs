﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AINetClient;
using System.Net;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.IO;

namespace FrontEnd.Models
{
    public class MyCookie
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public void DoSomething(bool ex) {
            if (ex) {
                throw new ArgumentException("Bad argument");
            }
            else {
                ErrorClient.WriteInfo("Doing something", null);
            }
        }

        public void DoSomethingElse() {
            string metricName = "cpu usage";
            double metricValue = 50;
            EventsClient.WriteEvent("Model", "msg here", metricName, metricValue.ToString(), null, null);
            IPEndPoint ep = RoleEnvironment.Roles["BackEnd"].Instances[0].InstanceEndpoints["BackendEndpoint"].IPEndpoint;
            Uri baseSvc = new Uri("http://"+ep.ToString());
            Random rnd = new Random();            
            Uri svc = new Uri(baseSvc, "/Home/Process/"+rnd.Next());
            HttpWebRequest req = WebRequest.Create(svc) as HttpWebRequest;
            ErrorClient.TransportSession(ref req);
            using (WebResponse res = req.GetResponse()) {
                using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                    string content = reader.ReadToEnd();
                    ErrorClient.WriteInfo(content, null);
                }
            }
        }
    }
}