﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Net;
using FrontEnd.Models;
using AINetClient;

namespace FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index() {
           
            try {
                ViewBag.Message = ActNow();
                //ViewBag.Message = "Welcome to ASP.NET MVC!";
            }
            catch (Exception e) {
                ErrorClient.WriteError("caught exception", e);
            }


            return View();
        }

        private string ActNow() {
            MyCookie mc = new MyCookie { Id = 1, Name = "Cookie Monster" };
            mc.DoSomethingElse();
            mc.DoSomething(false);
            mc.DoSomething(true);
            return mc.Name;
        }

        public ActionResult About() {
            return View();
        }
    }
}
