using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Diagnostics;

namespace FrontEnd
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart() {
            //IPEndPoint myEp = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["FrontendEndpoint"].IPEndpoint;
            IPEndPoint ep = RoleEnvironment.Roles["BackEnd"].Instances[0].InstanceEndpoints["BackendEndpoint"].IPEndpoint;
            //Trace.WriteLine("My endpoint is " + myEp.ToString());
            Trace.WriteLine("Endpoint of backend role instance is " + ep.ToString());

            return base.OnStart();
        }
    }
}
