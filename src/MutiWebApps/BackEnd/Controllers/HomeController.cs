﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AINetClient;

namespace BackEnd.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index() {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult Process(int id) {
            string metricName = "memory";
            string metricValue = "45";
            EventsClient.WriteEvent("Processor", "hahaha", metricName, metricValue, null, null);
            return Content("processed " + id);
        }

        public ActionResult About() {
            return View();
        }
    }
}
