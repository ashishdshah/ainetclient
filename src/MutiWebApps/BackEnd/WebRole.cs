using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Net;
using System.Diagnostics;

namespace BackEnd
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart() {
            //IPEndPoint ep = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["FrontendEndpoint"].IPEndpoint;
            IPEndPoint myEp = RoleEnvironment.Roles["BackEnd"].Instances[0].InstanceEndpoints["BackendEndpoint"].IPEndpoint;
            Trace.WriteLine("My endpoint is " + myEp.ToString());
            //Trace.WriteLine("Endpoint of frontend role instance is " + ep.ToString());
            
            return base.OnStart();
        }
    }
}
