﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.IO;

namespace AINetClient
{
    public class AppInsightsAppender : log4net.Appender.AppenderSkeleton
    {
        public string AiUrl { get; set; }

        public string AiApplicationKey { get; set; }

        public string AiCustomerKey { get; set; }

        protected override void Append(log4net.Core.LoggingEvent loggingEvent)
        {
            string sessionId = EventsClient.InitializeAiSession();
            EventsClient.Severity = CommonDefinitions.SeverityTypes.Debug;

            switch (loggingEvent.Level.DisplayName)
            {
                case "DEBUG":
                    EventsClient.WriteDebug(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;
                case "INFO":
                    EventsClient.WriteInfo(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;
                case "WARN":
                    EventsClient.WriteWarn(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;
                case "ERROR":
                    EventsClient.WriteError(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;

                case "FATAL":
                    EventsClient.WriteFatal(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;
                default:
                    EventsClient.WriteError(loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, loggingEvent.LocationInformation.ClassName);
                    break;
            };

        }
    }
}
