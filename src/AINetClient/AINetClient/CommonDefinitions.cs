﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AINetClient
{
    public class CommonDefinitions
    {
        public enum SeverityTypes
        {
            Fatal,
            Error,
            Warn,
            Info,
            Debug
        };

        // Event attributes to be sent to the server
        public const string METRIC_NAME = "MetricName";
        public const string METRIC_VALUE = "MetricValue";
        public const string SEVERITY_LABEL = "Severity.Label";
        public const string MESSAGE = "Message";
        public const string TIMESTAMP = "Timestamp";
        public const string COMPONENT_NAME = "ComponentName";
        public const string SESSION_ID = "SessionId";
        public const string CUSTOMER_KEY = "CustomerKey";
        public const string APPLICATION_KEY = "ApplicationKey";
        public const string CUSTOM_FIELDS_NAME = "CustomFields[{0}].Name";
        public const string CUSTOM_FIELDS_VALUE = "CustomFields[{0}].Value";

        // session id http header variable
        public const string HTTP_SESSION_ID = "AppInsightsHttpSessionId";

    }
}
