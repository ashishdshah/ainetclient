﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using System.IO;

namespace AINetClient
{
    class RestHelper
    {

        public static bool PostForm(string url, NameValueCollection formData)
        {
            // TBD Should NameValueCollection be urlencoded?

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            string formDataString = GetString(formData);
            
            byte[] bytedata = Encoding.UTF8.GetBytes(formDataString);
            webRequest.ContentLength = bytedata.Length;

            Stream requestStream = webRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
            if(webResponse.StatusCode == HttpStatusCode.OK)
                return true;
            else
                return false;

        }

        private static string GetString(NameValueCollection formData)
        {
            return string.Join("&", Array.ConvertAll(formData.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(formData[key]))));
        }
    }
}
