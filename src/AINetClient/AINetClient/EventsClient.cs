﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.Diagnostics;

namespace AINetClient
{
    public class EventsClient
    {
        // ThreadStatic will make this value unique per thread
        [ThreadStatic]
        static string _sessionId;


        public static string CustomerKey { get; set; }

        public static string ApplicationKey { get; set; }

        public static string Url { get; set; }

        public static CommonDefinitions.SeverityTypes Severity { get; set; }

        /// <summary>
        /// Call this for every session start
        /// 
        /// <returns></returns>
        public static string InitializeAiSession(HttpRequest httpReq)
        {
            if (httpReq == null)
            {
                // set session and request id in thread-local storage
                _sessionId = Guid.NewGuid().ToString();
            }
            else
            {
                // read from http header. if aisessionid is not present, then
                bool headerSessionPresent = (httpReq != null && !string.IsNullOrEmpty(httpReq.Headers[CommonDefinitions.HTTP_SESSION_ID]));

                if (headerSessionPresent)
                {
                    // get the sessionid from the header
                    _sessionId = httpReq.Headers[CommonDefinitions.HTTP_SESSION_ID];
                }
                else
                {
                    _sessionId = Guid.NewGuid().ToString();
                }

            }

            return _sessionId;
        }

        public static string InitializeAiSession()
        {
            return InitializeAiSession(null);
        }

        public static void EndAiSession()
        {
            _sessionId = null;
        }

        public static bool WriteEvent(string component,
                                string metricName,
                                double metricValue,
                                string message = "",
                                NameValueCollection customFields = null,
                                string sessionId = null)
        {
            // retrieve session id and request id from thread local
            string finalSessionId;

            if (string.IsNullOrEmpty(sessionId))
                finalSessionId = _sessionId;    // get from thread local storage
            else
                finalSessionId = sessionId;


            // For an event always treat severity as info
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Info;

            DateTime timeStamp = DateTime.UtcNow;

            NameValueCollection formCollection = new NameValueCollection();
            formCollection.Add(CommonDefinitions.METRIC_NAME, metricName);
            formCollection.Add(CommonDefinitions.METRIC_VALUE, metricValue.ToString());
            formCollection.Add(CommonDefinitions.MESSAGE, message);
            formCollection.Add(CommonDefinitions.COMPONENT_NAME, component);
            formCollection.Add(CommonDefinitions.SEVERITY_LABEL, sev.ToString());
            formCollection.Add(CommonDefinitions.TIMESTAMP, timeStamp.ToString());
            formCollection.Add(CommonDefinitions.CUSTOMER_KEY, CustomerKey);
            formCollection.Add(CommonDefinitions.APPLICATION_KEY, ApplicationKey);
            formCollection.Add(CommonDefinitions.SESSION_ID, finalSessionId);

            if (customFields == null)
                customFields = new NameValueCollection();

            int countFields = 0;
            foreach (string key in customFields)
            {
                string name = string.Format(CommonDefinitions.CUSTOM_FIELDS_NAME, countFields);
                formCollection.Add(name, key);

                string value = string.Format(CommonDefinitions.CUSTOM_FIELDS_VALUE, countFields);
                formCollection.Add(value, customFields[key]);
                countFields++;
            }
            
            // make the call to the REST API
            bool ret = RestHelper.PostForm(Url, formCollection);

            return ret;

        }

        #region Error interface

        public static void TransportSession(ref HttpWebRequest httpReq)
        {
            // add the threadlocal session to the httprequest
            bool headerSessionPresent = (httpReq != null && !string.IsNullOrEmpty(httpReq.Headers[CommonDefinitions.HTTP_SESSION_ID]));
            if (headerSessionPresent)
                httpReq.Headers[CommonDefinitions.HTTP_SESSION_ID] = _sessionId;
            else if (httpReq != null)
                httpReq.Headers.Add(CommonDefinitions.HTTP_SESSION_ID, _sessionId);

        }

        private static void WriteLog(string message, CommonDefinitions.SeverityTypes sev, Exception ex, string comp = "")
        {
            string sessionId = _sessionId;
            string component;

            if (string.IsNullOrEmpty(comp))
                component = GetComponent();
            else
                component = comp;

            DateTime timeStamp = DateTime.UtcNow;

            // add exception to message
            StringBuilder sb = new StringBuilder(message);
            sb.Append(GetExceptionDetails(ex));
            message = sb.ToString();

            // Call REST API
            NameValueCollection formCollection = new NameValueCollection();
            formCollection.Add(CommonDefinitions.MESSAGE, message);
            formCollection.Add(CommonDefinitions.COMPONENT_NAME, component);
            formCollection.Add(CommonDefinitions.SEVERITY_LABEL, sev.ToString());
            formCollection.Add(CommonDefinitions.TIMESTAMP, timeStamp.ToString());
            formCollection.Add(CommonDefinitions.CUSTOMER_KEY, CustomerKey);
            formCollection.Add(CommonDefinitions.APPLICATION_KEY, ApplicationKey);
            formCollection.Add(CommonDefinitions.SESSION_ID, sessionId);

            // make the call to the REST API
            bool ret = RestHelper.PostForm(Url, formCollection);
        }

        // TBD need to format this better
        private static string GetExceptionDetails(Exception ex)
        {
            string exDetails = "";
            if (ex == null)
                return exDetails;
            else
            {
                exDetails = ex.Message + ex.StackTrace;
                if (ex.InnerException != null)
                    exDetails += GetExceptionDetails(ex.InnerException);
            }

            return exDetails;
        }

        public static void WriteFatal(string message, Exception ex, string comp = "")
        {
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Fatal;

            WriteLog(message, sev, ex, comp);
        }

        public static void WriteError(string message, Exception ex, string comp = "")
        {
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Error;

            if (Severity == CommonDefinitions.SeverityTypes.Debug || Severity == CommonDefinitions.SeverityTypes.Info || Severity == CommonDefinitions.SeverityTypes.Warn || Severity == CommonDefinitions.SeverityTypes.Error)
                WriteLog(message, sev, ex, comp);
        }

        public static void WriteWarn(string message, Exception ex, string comp = "")
        {
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Warn;

            if (Severity == CommonDefinitions.SeverityTypes.Debug || Severity == CommonDefinitions.SeverityTypes.Info || Severity == CommonDefinitions.SeverityTypes.Warn)
                WriteLog(message, sev, ex, comp);
        }

        public static void WriteInfo(string message, Exception ex, string comp = "")
        {
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Info;

            if (Severity == CommonDefinitions.SeverityTypes.Info || Severity == CommonDefinitions.SeverityTypes.Debug)
                WriteLog(message, sev, ex, comp);
        }

        public static void WriteDebug(string message, Exception ex, string comp = "")
        {
            CommonDefinitions.SeverityTypes sev = CommonDefinitions.SeverityTypes.Info;

            if (Severity == CommonDefinitions.SeverityTypes.Debug)
                WriteLog(message, sev, ex, comp);
        }

        private static string GetComponent()
        {
            StackFrame frame = new StackFrame(3);
            string caller = frame.GetMethod().DeclaringType.FullName + "." + frame.GetMethod().Name;

            return caller;
        }


        #endregion
    }
}
