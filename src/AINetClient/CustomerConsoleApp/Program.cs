﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using AINetClient;
using System.IO;
using log4net;
using log4net.Config;


namespace CustomerConsoleApp
{
    class Program
    {
        internal static log4net.ILog log;

        static void Main(string[] args) 
        {
            var fi = new FileInfo("..\\..\\log4net.config");
            XmlConfigurator.Configure(fi);
            log = log4net.LogManager.GetLogger("CustomerConsoleApp");

            Thread bookThread = new Thread(ProcessBooks);
            bookThread.Start();

            Thread moreBooksThread = new Thread(ProcessMoreBooks);
            moreBooksThread.Start();
            
            // Use the Join method to block the current thread 
            // until the object's thread terminates.
            bookThread.Join();
            moreBooksThread.Join();

            Console.WriteLine("main thread: Worker thread has terminated.");
        }

        static void ProcessMoreBooks()
        {
            EventsClient.Url = "http://localhost:49540/Event";
            EventsClient.ApplicationKey = "app2key";
            EventsClient.CustomerKey = "key1";
            string sessionId = EventsClient.InitializeAiSession();

            EventsClient.Severity = CommonDefinitions.SeverityTypes.Debug;

            //EventsClient.WriteInfo("Starting 500 books", null);
            log.Info("Starting 500 books");

            for (int i = 0; i < 500; i++)
            {
                Book book = new Book(i);

                book.DoWork();
            }
        }

        static void ProcessBooks()
        {
            EventsClient.Url = "http://localhost:49540/Event";
            EventsClient.ApplicationKey = "app2key";
            EventsClient.CustomerKey = "key1";
            string sessionId = EventsClient.InitializeAiSession();

            EventsClient.Severity = CommonDefinitions.SeverityTypes.Debug;

            //EventsClient.WriteInfo("Starting 500 books", null);
            log.Info("Starting 500 books");

            for (int i = 0; i < 500; i++)
            {
                Book book = new Book(i);

                book.DoWork();
            }
        }

    }
}
