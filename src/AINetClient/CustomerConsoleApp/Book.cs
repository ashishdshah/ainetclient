﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using AINetClient;

namespace CustomerConsoleApp
{
    internal class Book
    {
        int _counter;

        public Book(int i)
        {
            _counter = i;
        }

        public void DoWork()
        {
            Random rnd = new Random();
            int pages = Convert.ToInt32(rnd.NextDouble() * 100);
            string pagesName = "Pages";
            int copiesSold = Convert.ToInt32(rnd.NextDouble() * 1000);
            string copiesSoldName = "CopiesSold";

            NameValueCollection bookFields = new NameValueCollection();
            bookFields.Add(pagesName, pages.ToString());
            bookFields.Add(copiesSoldName, copiesSold.ToString());


            EventsClient.WriteEvent("Book", "Working", (pages*2) + (rnd.NextDouble()*10), "", bookFields, null);
            SummarizeBook();
            PrintSummary();

            if(_counter % 2 == 0)
                PrintRawSummaryRatio();
        }

        private void PrintSummary()
        {
            EventsClient.WriteEvent("Book", "Printed", 1, "", null, null);
            if (_counter % 6 == 0)
            {
                try
                {
                    NeedAnError();
                }
                catch (Exception ex)
                {
                    //EventsClient.WriteError("From PrintSummary", new Exception("6th Record", ex));
                    Program.log.Error("From PrintSummary", new Exception("6th Record", ex));
                }
            }
        }

        private void NeedAnError()
        {
            try
            {
                NeedAnError2Deep();
            }
            catch (Exception ex)
            {
                throw new Exception("From 1 Deep", ex);
            }
        }

        private void NeedAnError2Deep()
        {
            throw new Exception("From 2 Deep");
        }

        private void PrintRawSummaryRatio()
        {
            Random rnd = new Random();

            double ratio = rnd.NextDouble();
            EventsClient.WriteEvent("Book", "RawSummaryRatio", ratio, "", null, null);
        }

        private void SummarizeBook()
        {
            EventsClient.WriteEvent("Book", "Summarized", 1, "", null, null);
        }
    }
}
